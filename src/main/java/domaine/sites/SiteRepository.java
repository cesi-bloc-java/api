package domaine.sites;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SiteRepository {

	private Connection con = null;

	public SiteRepository(){
		String url = "jdbc:mysql://localhost:3306/cesi_bloc";
		String userName = "root";
		String pwd = "";

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, userName, pwd);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public Site getSite(int id) throws SQLException {
		String query = "SELECT * FROM sites WHERE id = " + id;

		try (Statement st = this.con.createStatement();) {
			ResultSet rs = st.executeQuery(query);

			rs.next();

			int villeId = rs.getInt("id");
			String ville = rs.getString("ville");

			Site site = new Site();
			
			site.setId(villeId);
			site.setVille(ville);

			return site;

		} catch (NullPointerException | SQLException e) {
			return null;
		}
	}
	
	public List<Site> getSites() throws SQLException {

		List<Site> sitesList = new ArrayList<>();
		String query = "SELECT id FROM sites WHERE 1";
		try (Statement st = this.con.createStatement();) {
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");

				Site site = this.getSite(id);
				sitesList.add(site);
			}

			return sitesList;

		} catch (NullPointerException e) {
			return Collections.emptyList();
		}
	}
	
	public Site createSite(Site site) throws SQLException {

		String query = "INSERT INTO sites (ville) VALUES (?)";
		try (PreparedStatement st = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {

			st.setString(1,site.getVille());

			st.executeUpdate();
			ResultSet generatedKey = st.getGeneratedKeys();
			generatedKey.next();

			System.out.println("site created");
			return this.getSite(generatedKey.getInt(1));

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to create site " + site);
			return null;
		}
	}
	
	public void updateSite(Site site) throws SQLException {
		String query = "UPDATE sites SET ville = ? WHERE id = ?";
		try (PreparedStatement st = con.prepareStatement(query);) {

			st.setString(1, site.getVille());
			st.setInt(2, site.getId());

			st.executeUpdate();

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to update site " + site);
		}
	}
	
	public void deleteSite(int id) throws SQLException {
		String query = "DELETE FROM sites WHERE id = ?";
		try (PreparedStatement st = con.prepareStatement(query);) {

			System.out.println("deleteSite");
			
			st.setInt(1, id);
			st.executeUpdate();

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to delete site " + id);
		}
	}

}
