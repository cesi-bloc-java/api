package domaine.sites;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("sites")
public class SiteResource {

	private SiteRepository repository = new SiteRepository();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Site> getSites() throws SQLException {
		return repository.getSites();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Site getSite(@PathParam("id") int id) throws SQLException {
		System.out.println("getSite() called from siteResource");
		return repository.getSite(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void createSite(Site site) throws SQLException {
		repository.createSite(site);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateSite(Site site) throws SQLException {
		if (repository.getSite(site.getId()) != null) {
			repository.updateSite(site);
		}
	}
	
	@DELETE
	@Path("/{id}")
	public void deleteClient(@PathParam("id") int id) throws SQLException {

		Site site = repository.getSite(id);
		if (repository.getSite(site.getId()) != null) {
			repository.deleteSite(site.getId());
		}
	}
	
}
