package domaine.sites;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Site {
	
	private int id;
	private String ville;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	@Override
	public String toString() {
		return "Site [id=" + id + ", ville=" + ville + "]";
	}
}
