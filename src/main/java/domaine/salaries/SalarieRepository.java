package domaine.salaries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SalarieRepository {

	private Connection con = null;

	public SalarieRepository(){
		String url = "jdbc:mysql://localhost:3306/cesi_bloc";
		String userName = "root";
		String pwd = "";

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, userName, pwd);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public Salarie getSalarie(int id) throws SQLException {
		
		String query = "SELECT * FROM salaries WHERE id = " + id;
//		String query = "SELECT salaries.id, salaries.nom, salaries.prenom, salaries.telFixe, salaries.telPortable, salaries.email, services.nom AS service, sites.ville AS site FROM salaries JOIN services ON salaries.service = services.id JOIN sites ON salaries.site = sites.id WHERE salaries.id = " + id;

		try (Statement st = this.con.createStatement();) {
			ResultSet rs = st.executeQuery(query);

			rs.next();
			
			int salarieId = rs.getInt("id");
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			String telFixe = rs.getString("telFixe");
			String telPortable  = rs.getString("telPortable");
			String email = rs.getString("email");
			int service = rs.getInt("service");
			int site = rs.getInt("site");

			Salarie salarie = new Salarie();
			
			salarie.setId(salarieId);
			salarie.setNom(nom);
			salarie.setPrenom(prenom);
			salarie.setTelFixe(telFixe);
			salarie.setTelPortable(telPortable);
			salarie.setEmail(email);
			salarie.setService(service);
			salarie.setSite(site);
			
			return salarie;

		} catch (NullPointerException | SQLException e) {
			return null;
		}
	}
	
	public List<Salarie> getSalaries() throws SQLException {

		List<Salarie> salarieList = new ArrayList<>();
		String query = "SELECT id FROM salaries WHERE 1";
		try (Statement st = this.con.createStatement();) {
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");

				Salarie salarie = this.getSalarie(id);
				salarieList.add(salarie);
			}

			return salarieList;

		} catch (NullPointerException e) {
			return Collections.emptyList();
		}
	}
	
	public Salarie createSalarie(Salarie salarie) throws SQLException {

		String query = "INSERT INTO salaries (nom, prenom, telFixe, telPortable, email, service, site) VALUES (?, ?, ?, ?, ?, ?, ?)";
		try (PreparedStatement st = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {

			st.setString(1, salarie.getNom());
			st.setString(2, salarie.getPrenom());
			st.setString(3, salarie.getTelFixe());
			st.setString(4, salarie.getTelPortable());
			st.setString(5, salarie.getEmail());
			st.setInt(6, salarie.getService());
			st.setInt(7, salarie.getSite());

			st.executeUpdate();
			ResultSet generatedKey = st.getGeneratedKeys();
			generatedKey.next();

			System.out.println("salarie created");
			return this.getSalarie(generatedKey.getInt(1));

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to create salarie " + salarie);
			return null;
		}
	}
	
	public void updateSalarie(Salarie salarie) throws SQLException {
		String query = "UPDATE salaries SET nom = ?, prenom = ?, telFixe = ?, telPortable = ?, email = ?, service = ?, site = ? WHERE id = ?";
		try (PreparedStatement st = con.prepareStatement(query);) {
			
			st.setString(1, salarie.getNom());
			st.setString(2, salarie.getPrenom());
			st.setString(3, salarie.getTelFixe());
			st.setString(4, salarie.getTelPortable());
			st.setString(5, salarie.getEmail());
			st.setInt(6, salarie.getService());
			st.setInt(7, salarie.getSite());
			st.setInt(8, salarie.getId());

			st.executeUpdate();

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to update salarie " + salarie);
		}
	}
	
	public void deleteSalarie(int id) throws SQLException {
		String query = "DELETE FROM salaries WHERE id = ?";
		try (PreparedStatement st = con.prepareStatement(query);) {

			System.out.println("delete Salarie");
			
			st.setInt(1, id);
			st.executeUpdate();

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to delete salarie " + id);
		}
	}

}
