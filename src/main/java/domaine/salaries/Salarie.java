package domaine.salaries;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Salarie {
	
	private int id;
	private String nom;
	private String prenom;
	private String telFixe;
	private String telPortable;
	private String email;
	private int service;
	private int site;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getTelFixe() {
		return telFixe;
	}
	public void setTelFixe(String telFixe) {
		this.telFixe = telFixe;
	}
	public String getTelPortable() {
		return telPortable;
	}
	public void setTelPortable(String telPortable) {
		this.telPortable = telPortable;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getService() {
		return service;
	}
	public void setService(int service) {
		this.service = service;
	}
	public int getSite() {
		return site;
	}
	public void setSite(int site) {
		this.site = site;
	}
	
	@Override
	public String toString() {
		return "Salarie [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", telFixe=" + telFixe + ", telPortable="
				+ telPortable + ", email=" + email + ", service=" + service + ", site=" + site + "]";
	}
	
}
