package domaine.salaries;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("salaries")
public class SalarieResource {

	private SalarieRepository repository = new SalarieRepository();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Salarie> getSalaries() throws SQLException {
		return this.repository.getSalaries();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Salarie getSalarie(@PathParam("id") int id) throws SQLException {
		System.out.println("getSalarie() called from salarieResource");
		return this.repository.getSalarie(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void createSalarie(Salarie salarie) throws SQLException {
		System.out.println("getSalaries() called from salarieResource");
		this.repository.createSalarie(salarie);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateSalarie(Salarie salarie) throws SQLException {
		if (repository.getSalarie(salarie.getId()) != null) {
			this.repository.updateSalarie(salarie);
		}
	}
	
	@DELETE
	@Path("/{id}")
	public void deleteSalarie(@PathParam("id") int id) throws SQLException {

		Salarie salarie = repository.getSalarie(id);
		if (repository.getSalarie(salarie.getId()) != null) {
			this.repository.deleteSalarie(salarie.getId());
		}
	}
	
}
