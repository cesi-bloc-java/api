package domaine.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("services")
public class ServiceResource {

	private ServiceRepository repository = new ServiceRepository();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Service> getServices() throws SQLException {
		return this.repository.getServices();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Service getService(@PathParam("id") int id) throws SQLException {
		System.out.println("getService() called from serviceResource");
		return this.repository.getService(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void createService(Service service) throws SQLException {
		this.repository.createService(service);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateService(Service service) throws SQLException {
		if (repository.getService(service.getId()) != null) {
			this.repository.updateService(service);
		}
	}
	
	@DELETE
	@Path("/{id}")
	public void deleteService(@PathParam("id") int id) throws SQLException {

		Service service = repository.getService(id);
		if (repository.getService(service.getId()) != null) {
			this.repository.deleteService(service.getId());
		}
	}
	
}
