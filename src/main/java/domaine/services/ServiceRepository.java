package domaine.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServiceRepository {

	private Connection con = null;

	public ServiceRepository(){
		String url = "jdbc:mysql://localhost:3306/cesi_bloc";
		String userName = "root";
		String pwd = "";

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, userName, pwd);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public Service getService(int id) throws SQLException {
		String query = "SELECT * FROM services WHERE id = " + id;

		try (Statement st = this.con.createStatement();) {
			ResultSet rs = st.executeQuery(query);

			rs.next();

			int serviceId = rs.getInt("id");
			String nom = rs.getString("nom");

			Service service = new Service();
			
			service.setId(serviceId);
			service.setNom(nom);

			return service;

		} catch (NullPointerException | SQLException e) {
			return null;
		}
	}
	
	public List<Service> getServices() throws SQLException {

		List<Service> serviceList = new ArrayList<>();
		String query = "SELECT id FROM services WHERE 1";
		try (Statement st = this.con.createStatement();) {
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");

				Service service = this.getService(id);
				serviceList.add(service);
			}

			return serviceList;

		} catch (NullPointerException e) {
			return Collections.emptyList();
		}
	}
	
	public Service createService(Service service) throws SQLException {

		String query = "INSERT INTO services (nom) VALUES (?)";
		try (PreparedStatement st = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {

			st.setString(1,service.getNom());

			st.executeUpdate();
			ResultSet generatedKey = st.getGeneratedKeys();
			generatedKey.next();

			System.out.println("service created");
			return this.getService(generatedKey.getInt(1));

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to create service " + service);
			return null;
		}
	}
	
	public void updateService(Service service) throws SQLException {
		String query = "UPDATE services SET nom = ? WHERE id = ?";
		try (PreparedStatement st = con.prepareStatement(query);) {

			st.setString(1, service.getNom());
			st.setInt(2, service.getId());

			st.executeUpdate();

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to update service " + service);
		}
	}
	
	public void deleteService(int id) throws SQLException {
		String query = "DELETE FROM services WHERE id = ?";
		try (PreparedStatement st = con.prepareStatement(query);) {

			System.out.println("delete service");
			
			st.setInt(1, id);
			st.executeUpdate();

		} catch (NullPointerException | SQLException e) {
			System.out.println("error while trying to delete service " + id);
		}
	}

}
